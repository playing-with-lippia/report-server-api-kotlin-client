/**
 * Lippia Report Server API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
package playing.with.lippia.reportserver.model


/**
 * 
 * @param projectName 
 * @param reportName 
 * @param reporters 
 */
data class ReportInfo (

    val projectName: kotlin.String? = null,
    val reportName: kotlin.String? = null,
    val reporters: kotlin.Any? = null
) {
}